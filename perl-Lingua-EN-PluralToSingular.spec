%global cpan_name Lingua-EN-PluralToSingular
%global _empty_manifest_terminate_build 0

Name:           perl-%{cpan_name}
Version:        0.21
Release:        2
Summary:        Change an English plural to a singular

License:        Perl License
URL:            http://search.cpan.org/dist/%{cpan_name}/
Source0:        http://www.cpan.org/authors/id/B/BK/BKB/%{cpan_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl >= 0:5.006001
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker)

%description
This converts words denoting a plural in the English language into words
denoting a singular noun.


%package help
Summary : Change an English plural to a singular
Provides: perl-%{cpan_name}-doc
%description help
This converts words denoting a plural in the English language into words
denoting a singular noun.


%prep
%setup -q -n %{cpan_name}-%{version}


%build
export PERL_MM_OPT=""
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}


%install
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

%check
make test

%files
%doc Changes META.json README
%{_bindir}/*
%{perl_vendorlib}/*

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 0.21-2
- cleanup spec

* Wed Jul 14 2021 ice-kylin <wminid@yeah.net> 0.21-1
- Initial package
